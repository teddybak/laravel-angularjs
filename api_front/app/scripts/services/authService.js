/**
 * Created by mohtadi on 17/03/17.
 */
angular.module('authService',[]).factory('sessionControl',function(){

    return {
           get:function(key){
               return sessionStorage.getItem(key);
           },
           set:function(key,val){
                return sessionStorage.setItem(key,val);
           },
           unset: function (key) {
               return sessionStorage.removeItem(key);
           }
       };
    })

    .factory('authUser',function ($auth,sessionControl,toastr,$location) {

        var cacheSession = function (email,username,avatar){
            sessionControl.set('userIsLogin',true);
            sessionControl.set('email',email);
            sessionControl.set('username',username);
            sessionControl.set('avatar',avatar);
        };

        var uncacheSession = function(){
            sessionControl.unset('userIsLogin');
            sessionControl.unset('email');
            sessionControl.unset('username');
            sessionControl.unset('avatar');
        };

        var login = function (loginForm) {
            $auth.login(loginForm).then(
                function (response){
                    console.log("session iniciada con exito");
                    cacheSession(response.data.user.email,response.data.user.username,loginForm.avatar);
                    $location.path('/')
                    toastr.success('Session Iniciada','Mensaje');
                },function (error) {
                    uncacheSession();
                    toastr.error(error.data.error,'Error');
                    console.log(error);
                }
            );
        };

        return{
            loginApi : function(loginForm){
                login(loginForm);
            },
            logout: function () {
                $auth.logout();
                uncacheSession();
                //toastr.success('Session Finalizada','Mensaje');
                $location.path('/');
            },
           isLoggedIn: function(){
                return  sessionControl.get('userIsLogin') !== null;
           }
        }


    });