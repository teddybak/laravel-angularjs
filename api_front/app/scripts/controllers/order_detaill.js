/**
 * Created by mohtadi on 19/03/17.
 */
'use strict';

/**
 * @ngdoc function
 * @name apiFrontApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the apiFrontApp
 */
angular.module('apiFrontApp').controller('OrderDetaillCtrl', function ($scope, $routeParams,$http) {

    var vm = this;
    vm.menuTemplate = {
        url:'views/menu.html'
    };

    $http({method:'GET', url:'http://local.laravel5p2.com/api/v1/order/'+ $routeParams.id})
        .then(function(response) {
            $scope.data = response.data[0]
        }, function(error) {
            console.log("error");
        });
});
