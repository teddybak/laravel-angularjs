'use strict';

/**
 * @ngdoc function
 * @name apiFrontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the apiFrontApp
 */
angular.module('apiFrontApp')
    .controller('OrderCtrl',function ($scope,$http) {

        $scope.menuTemplate = {
            url:'views/menu.html'
        };

        $http.get("http://local.laravel5p2.com/api/v1/order")
            .then(function(response) {
                $scope.data = response.data;
            });
    });
