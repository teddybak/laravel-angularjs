/**
 * Created by mohtadi on 20/03/17.
 */
'use strict';

/**
 * @ngdoc function
 * @name apiFrontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the apiFrontApp
 */
angular.module('apiFrontApp').controller('NewRetailerCtrl',function ($scope,$http,toastr,$location) {

    $scope.menuTemplate = {
        url:'views/menu.html'
    };



    $scope.sendPost = function () {
        // send login data

        $http({
            method: 'POST',
            url: 'http://local.laravel5p2.com/api/v1/retailer',
            data: ({
                name: $scope.name,
                location: $scope.location,
                email: $scope.email,
                secret:$scope.secret
            }),
            headers: {'Content-Type': 'application/json'}
        }).then(function (result) {
            $location.path('/')
            toastr.success('New retailer has been created','Mensaje');

        },function (result) {

        });
    };

});/**
 * Created by mohtadi on 22/03/17.
 */
