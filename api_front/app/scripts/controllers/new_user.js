/**
 * Created by mohtadi on 20/03/17.
 */
'use strict';

/**
 * @ngdoc function
 * @name apiFrontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the apiFrontApp
 */
angular.module('apiFrontApp').controller('NewUserCtrl',function ($scope,$http,toastr,$location) {

    $scope.menuTemplate = {
        url:'views/menu.html'
    };



    $scope.sendPost = function () {
        // send login data

        $http({
            method: 'POST',
            url: 'http://local.laravel5p2.com/api/v1/user',
            data: ({
                firstName: $scope.firstname,
                lastName: $scope.lastname,
                email:$scope.email,
                role_id: $scope.role_id,
                username:$scope.username,
                password:$scope.password
            }),
            headers: {'Content-Type': 'application/json'}
        }).then(function (result) {

            $location.path('/')
            toastr.success('New User has been created','Mensaje');

        },function (result) {

        });
    };

});/**
 * Created by mohtadi on 22/03/17.
 */
