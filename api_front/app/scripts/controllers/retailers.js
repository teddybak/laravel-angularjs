/**
 * Created by mohtadi on 22/03/17.
 */
/**
 * Created by mohtadi on 20/03/17.
 */
'use strict';

/**
 * @ngdoc function
 * @name apiFrontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the apiFrontApp
 */
angular.module('apiFrontApp').controller('RetailerCtrl',function ($scope,$http,toastr,$location) {

    $scope.menuTemplate = {
        url:'views/menu.html'
    };

    $http.get("http://local.laravel5p2.com/api/v1/retailer")
        .then(function(response) {
            $scope.data = response.data;
        });



});