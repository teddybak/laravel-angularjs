/**
 * Created by mohtadi on 23/03/17.
 */
/**
 * Created by mohtadi on 20/03/17.
 */
'use strict';

/**
 * @ngdoc function
 * @name apiFrontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the apiFrontApp
 */
angular.module('apiFrontApp').controller('OrderCustomerDetailCtrl',function ($scope,$http,toastr,$location,$routeParams) {

    $scope.menuTemplate = {
        url:'views/menu.html'
    };


    console.log($routeParams.id);

    $http({method:'GET', url:'http://local.laravel5p2.com/api/v1/order/customer/'+ $routeParams.id})
        .then(function(response) {
            $scope.data = response.data["orders_customer"];
            $scope.data.customer_detaills = response.data["customer_detaill"]

        }, function(error) {
            console.log("error");
        });


});