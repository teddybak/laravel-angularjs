/**
 * Created by mohtadi on 17/03/17.
 */
angular.module('apiFrontApp').controller('MenuCtrl', function (authUser,$location,$scope,sessionControl) {

    $scope.isLogin = authUser.isLoggedIn();


    function DropdownController() {
        var vm = this;

        vm.isCollapsed = true;
        vm.status = {
            isopen: false
        }
    };


    $scope.$watch(function () {
       return  authUser.isLoggedIn();
    },function (newVal) {
        if(typeof newVal !== 'undefined'){
            $scope.isLogin = authUser.isLoggedIn() ;
        }
    });

    $scope.$watch(function () {
       return sessionControl.get('username');
    },function (newVal) {
        if(typeof newVal !== 'undefined'){
            $scope.user.name = sessionControl.get('username') ;
        }
    });

    $scope.$watch(function () {
        return sessionControl.get('email');
    },function (newVal) {
        if(typeof newVal !== 'undefined'){
            $scope.user.name = sessionControl.get('email') ;
        }
    });



    $scope.user = {
        email:sessionControl.get('email'),
        username:sessionControl.get('username'),
        avatar:sessionControl.get('avatar')
    };

    $scope.logout = function () {
        console.log("session cerrada");
        authUser.logout();
    };

    $scope.isActive = function (viewLocation) {
        return viewLocation === $location.path();
    };

    });
/**
 * Created by mohtadi on 17/03/17.
 */
