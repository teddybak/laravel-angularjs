/**
 * Created by mohtadi on 20/03/17.
 */
'use strict';

/**
 * @ngdoc function
 * @name apiFrontApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the apiFrontApp
 */
angular.module('apiFrontApp').controller('NewOrderCtrl',function ($scope,$http,toastr,$location) {

        $scope.menuTemplate = {
            url:'views/menu.html'
        };

        $http.get("http://local.laravel5p2.com/api/v1/customer")
        .then(function(response) {
            $scope.data_customer = response.data;
        }); 

        $http.get("http://local.laravel5p2.com/api/v1/retailer")
        .then(function(response) {
            $scope.data_retailers = response.data;
        });         


        $scope.sendPost = function ( ) { 

            $http({
                method: 'POST',
                url: 'http://local.laravel5p2.com/api/v1/order',
                data: ({
                        customer_id: $scope.data.model,
                        retailer_id: $scope.data.model2,
                        status: $scope.status,
                        total:$scope.total
                }),
                headers: {'Content-Type': 'application/json'}
            }).then(function (result) {
                $location.path('/')
                toastr.success('New Order has been created','Mensaje');
            
            },function (result) {
            
            });
        };

});