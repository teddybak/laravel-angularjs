'use strict';

/**
 * @ngdoc overview
 * @name apiFrontApp
 * @description
 * # apiFrontApp
 *
 * Main module of the application.
 */
angular
  .module('apiFrontApp', [
    'authService',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'satellizer',
    'toastr',
    'ui.bootstrap',
    'ngAnimate'


  ])
  .config(function ($routeProvider, $authProvider) {
      $authProvider.loginUrl = 'http://local.laravel5p2.com/api/v1/auth_login'

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/user', {
        templateUrl: 'views/user.html',
        controller: 'UserCtrl',
        controllerAs: 'user'
      })
        .when('/new_user', {
            templateUrl: 'views/new_user.html',
            controller: 'NewUserCtrl',
            controllerAs: 'new_user'
        })
        .when('/order', {
            templateUrl: 'views/order.html',
            controller: 'OrderCtrl',
            controllerAs: 'order'
        })
        .when('/order_customer', {
            templateUrl: 'views/order_customer.html',
            controller: 'OrderCustomerCtrl',
            controllerAs: 'order_customer'
        })
        .when('/order_customer_detail/:id', {
            templateUrl: 'views/order_customer_detail.html',
            controller: 'OrderCustomerDetailCtrl',
            controllerAs: 'order_customer_detail'
        })
        .when('/new_order', {
            templateUrl: 'views/new_order.html',
            controller: 'NewOrderCtrl',
            controllerAs: 'new_order'
        })
        .when('/order_detaill/:id', {
            templateUrl: 'views/order_detaill.html',
            controller: 'OrderDetaillCtrl',
            controllerAs: 'detaill'
        })


        .when('/retailers', {
            templateUrl: 'views/retailers.html',
            controller: 'RetailerCtrl',
            controllerAs: 'retailer'
        })

        .when('/new_retailer', {
            templateUrl: 'views/new_retailer.html',
            controller: 'NewRetailerCtrl',
            controllerAs: 'new_retailer'
        })

        .when('/customers', {
            templateUrl: 'views/customers.html',
            controller: 'CustomersCtrl',
            controllerAs: 'customers'
        })


        .when('/login', {
            templateUrl: 'views/login.html',
            controller: 'LoginCtrl',
            controllerAs: 'login'
        })
      .otherwise({
        redirectTo: '/'
      });


  })
.run(function ($rootScope,$location,authUser,toastr) {
    var str = "order_detaill/4";
    str = (str.substr(0,14));

    console.log($location["$$path"]);

    var rutasPrivadas = ['/', str ,'/order','/user'];

    $rootScope.$on('$routeChangeStart',function(){
        if(($.inArray($location.path(),rutasPrivadas) !== -1) && !authUser.isLoggedIn()){
            toastr.error('Debe iniciar sesion para poder continuar','Mensaje');
            $location.path('/login');
        }
    })

})

