<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Customer extends Model
{
    protected $table = "customers";

    public static function getAllCustomers(){
        return Customer::all();
    }
}
