<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use App\Http\Controllers\ApiAuthController;

Route::get('/', function () {
    return view('welcome');
});





Route::group(['middleware' => 'cors','prefix' => 'api/v1'], function () {

    Route::post('auth_login', 'ApiAuthController@userAuth' );

    Route::resource('user', 'UserController');
    Route::resource('role', 'RoleController');
    Route::resource('retailer', 'RetailerController');
    Route::resource('order', 'OrderController');
    Route::resource('customer', 'CustomerController');

    Route::get('order/customer/{id}','OrderController@getOrderCustomer');
    Route::get('order/status/{status}','OrderController@getOrdersStatus');
    Route::put('order/{id}', 'OrderController@update');


});




Route::get('test/{id}',function ($id ){


    $orders_user = \Illuminate\Support\Facades\DB::table('orders')
        ->join('retailers', 'retailers.id', '=', 'orders.retailer_id')
        ->join('users', 'users.id', '=', 'orders.user_id')
        ->select('retailers.*', 'users.*', 'orders.*','retailers.email as retamail')
        ->where('users.id',$id)
        ->get();

    $user_detaill = array ();

    foreach ($orders_user as $key => $value){
        foreach ($value as $key2 => $value2){
            if($key2 == 'firstName'){
                array_push($user_detaill,$value2);
                continue;
            }

            if ($key2 == 'lastName'){
                array_push($user_detaill,$value2);
                continue;
            }

            if(count($user_detaill) > 1){
                break;
            }
        }
    }

    $json_return ["order_user"] = $orders_user;
    $json_return ["user_detaill"] = $user_detaill;

    return $json_return;
});